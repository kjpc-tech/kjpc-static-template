def test_generation_with_defaults(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_static_template_test',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()


def test_generation_with_no_debug(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_static_template_test',
        'debug': 'n',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()


def test_generation_with_vuejs(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_static_template_test',
        'use_vuejs': 'y',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()
