import os

from invoke import task, Collection


current_dir = os.path.dirname(os.path.abspath(__file__))
venv = os.path.join(current_dir, "venv")
ansible_dir = os.path.join(current_dir, "ansible")

# make sure this is the same as `main_user_name` in `ansible/vars/secrets.yml`
ansible_default_user = 'webber'


def _run_command(ctx, command, directory=current_dir):
    print(f"Running '{command}'.")
    with ctx.prefix(f'source {venv}/bin/activate'):
        with ctx.cd(directory):
            ctx.run(command, pty=True)


@task
def runbuild(ctx):
    """
    Run the cactus build command.
    """
    cmd = 'cactus build'
    _run_command(ctx, cmd)


@task
def runserver(ctx):
    """
    Run the cactus serve command.
    """
    cmd = 'cactus serve'
    _run_command(ctx, cmd)


@task
def runwebpack(ctx, debug=True, watch=True):
    """
    Run the webpack command.
    """
    cmd = 'npm run build'
    if debug:
        cmd += '-dev'
    if watch:
        cmd += '-watch'
    _run_command(ctx, cmd)


@task(post=[runbuild])
def setup(ctx):
    """
    Setup the development environment.
    """
    # create virtual environment if it doesn't exist
    if not os.path.exists(os.path.join(venv, "bin", "activate")):
        print(f"Creating virtual environment '{venv}'.")
        ctx.run(f'python3 -m venv {venv}', pty=True)
    else:
        print(f"Virtual environment '{venv}' not created because it already exists.")

    # install dependencies
    with ctx.prefix(f'source {venv}/bin/activate'):
        with open(os.path.join(current_dir, "requirements", "pinned.txt")) as f:
            has_pinned_dependencies = 'cactus' in f.read()
        if has_pinned_dependencies:
            # install pinned dependencies
            print("Installing dependencies from requirements/pinned.txt.")
            ctx.run('pip install -r requirements/pinned.txt', pty=True)
        else:
            # install normal dependencies
            print("Installing dependencies from requirements/base.txt.")
            ctx.run('pip install -r requirements/base.txt', pty=True)

            # freeze dependencies
            print("Freezing dependencies into requirements/pinned.txt.")
            ctx.run('pip freeze | grep -v "pkg-resources" > requirements/pinned.txt', pty=True)

    # install node modules
    with ctx.prefix(f'source {venv}/bin/activate'):
        print("Creating node environment.")
        ctx.run('nodeenv --python-virtualenv', pty=True)
        print("Installing node package.")
        if os.path.exists(os.path.join(current_dir, "package-lock.json")):
            ctx.run('npm ci', pty=True)
        else:
            ctx.run('npm install', pty=True)
        print("Building initial assets.")
        runwebpack(ctx, debug=True, watch=False)


@task
def deploy_setup(ctx):
    """
    First time setup for deployment. Add required submodules.
    """
    ansible_relative_dir = ansible_dir.replace(current_dir, '.')
    for repo_url, repo_path in [
        ("https://gitlab.com/kjpc-tech/kjpc-ansible-roles.git", os.path.join(ansible_relative_dir, "vendor", "kjpc-ansible-roles")),
        ("https://gitlab.com/cdetar/cfd-common-roles.git", os.path.join(ansible_relative_dir, "vendor", "cfd-common-roles")),
    ]:
        cmd = f'git submodule add {repo_url} {repo_path}'
        ctx.run(cmd, pty=True)


@task
def deploy(ctx, user=ansible_default_user, tags=None):
    """
    Deploy {{ cookiecutter.project_name }}.
    """
    tags = f'--tags {tags}' if tags is not None else ''
    cmd = f'ansible-playbook --inventory hosts.yml deploy-prod.yml --user {user} --ask-vault-pass {tags}'
    _run_command(ctx, cmd, directory=ansible_dir)


@task
def remote_update(ctx, user=ansible_default_user):
    """
    Update the remote server(s).
    """
    cmd = f'ansible-playbook --inventory hosts.yml updates.yml --user {user} --ask-vault-pass'
    _run_command(ctx, cmd, directory=ansible_dir)


@task
def remote_reboot(ctx, user=ansible_default_user):
    """
    Reboot the remote server(s).
    """
    cmd = f'ansible-playbook --inventory hosts.yml reboot.yml --user {user} --ask-vault-pass'
    _run_command(ctx, cmd, directory=ansible_dir)


site = Collection('site')
site.add_task(setup)
site.add_task(runbuild)
site.add_task(runserver)
site.add_task(runwebpack)

remote = Collection('remote')
remote.add_task(remote_update, 'update')
remote.add_task(remote_reboot, 'reboot')
remote.add_task(deploy_setup, 'setup')
remote.add_task(deploy)

namespace = Collection()
namespace.add_collection(site)
namespace.add_collection(remote)
