import 'vue/dist/vue.min.js';
import VueResource from 'vue-resource';

Vue.use(VueResource);

// Other useful packages
//   - vue-loader
//   - bootstrap-vue
//   - vue-cookie
//   - vue-router
//   - vue-native-websocket
//   - vuedraggable
//   - vue-feather-icons
