import os

from cactus.template_tags import config


def preBuildPage(page, context, data):
    """
    Updates the context of the page to include: the page itself as
    """

    # This will run for each page that Cactus renders.
    # Any changes you make to context will be passed to the template renderer for this page.

    # directory where bundles can be found
    bundles_dir = os.path.join(
        os.path.dirname(os.path.dirname(os.path.abspath(__file__))),
        config(context, 'BUNDLE_DIR')
    )

    # bundles to include in the site, should be ordered
    bundles_names = config(context, 'BUNDLE_NAMES')

    bundles_css = []
    bundles_js = []

    # go through bundle directory and find latest bundles
    for bundle_index, bundle_name in enumerate(bundles_names):
        bundles_css.append(None)
        bundles_js.append(None)
        for file_name in os.listdir(bundles_dir):
            if file_name.startswith(bundle_name):
                modification_time = os.path.getmtime(os.path.join(bundles_dir, file_name))
                if file_name.endswith('.css') and (bundles_css[bundle_index] is None or modification_time >= bundles_css[bundle_index][1]):
                    bundles_css[bundle_index] = ("./static/bundles/{}".format(file_name), modification_time)
                if file_name.endswith('.js') and (bundles_js[bundle_index] is None or modification_time >= bundles_js[bundle_index][1]):
                    bundles_js[bundle_index] = ("./static/bundles/{}".format(file_name), modification_time)

    extra = {
        "CURRENT_PAGE": page,
        # Add your own dynamic context elements here!
        'is_debug': config(context, 'DEBUG') or False,
        'bundles_css': bundles_css,
        'bundles_js': bundles_js,
    }

    context.update(extra)

    return context, data
