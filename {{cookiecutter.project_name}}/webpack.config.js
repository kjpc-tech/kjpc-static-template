const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const precss = require('precss');
const autoprefixer = require('autoprefixer');

const DEBUG = process.env.NODE_ENV !== 'production';

module.exports = {
  context: __dirname,
  mode: 'none',
  entry: {
    index: './webpack/src/index.js',{% if cookiecutter.use_vuejs == "y" %}
    vueapp: './webpack/src/vueapp.js',{% endif %}
    mainapp: './webpack/src/mainapp.js',
  },
  output: {
    filename: DEBUG ? '[name].js' : '[name].[hash].js',
    path: path.resolve('./static/bundles/'),
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: DEBUG ? '[name].css' : '[name].[hash].css',
      path: path.resolve('./static/bundles/'),
    }),
  ],
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  },
  module: {
    rules: [{
      test: /\.(sa|sc|c)ss$/,
      use: [
        MiniCssExtractPlugin.loader,
        'css-loader',
        {
          loader: 'postcss-loader',
          options: {
            plugins() {
              return [
                precss,
                autoprefixer,
              ];
            },
          },
        },
        'sass-loader',
      ],
    }, {
      test: require.resolve('jquery'),
      use: [{
        loader: 'expose-loader',
        options: 'jQuery'
      }, {
        loader: 'expose-loader',
        options: '$'
      }]
    }, {
      test: require.resolve('feather-icons/dist/feather.min.js'),
      use: [{
        loader: 'expose-loader',
        options: 'feather'
      }]
    }{% if cookiecutter.use_vuejs == "y" %}, {
      test: require.resolve('vue/dist/vue.min.js'),
      use: [{
        loader: 'expose-loader',
        options: 'Vue'
      }]
    }{% endif %}],
  },
};
